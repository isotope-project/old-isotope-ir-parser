/*!
An AST for isotope IR
*/
use super::*;

use ibig::UBig;
use smallvec::SmallVec;
use smol_str::SmolStr;

use std::{cmp::Ordering, ops::Range};

/// A source ID
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct SourceId(pub u64); //TODO: think about this

/// A spanned node
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Spanned<T> {
    /// The spanned node
    pub inner: T,
    /// The node's span
    pub span: (SourceId, Range<usize>),
}

impl<T: PartialOrd> PartialOrd for Spanned<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self.span.0.cmp(&other.span.0) {
            Ordering::Equal => {}
            cmp => return Some(cmp),
        }
        match self.span.1.start.cmp(&other.span.1.start) {
            Ordering::Equal => {}
            cmp => return Some(cmp),
        }
        match self.span.1.end.cmp(&other.span.1.end) {
            Ordering::Equal => {}
            cmp => return Some(cmp),
        }
        self.inner.partial_cmp(&other.inner)
    }
}

impl<T: Ord> Ord for Spanned<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.span.0.cmp(&other.span.0) {
            Ordering::Equal => {}
            cmp => return cmp,
        }
        match self.span.1.start.cmp(&other.span.1.start) {
            Ordering::Equal => {}
            cmp => return cmp,
        }
        match self.span.1.end.cmp(&other.span.1.end) {
            Ordering::Equal => {}
            cmp => return cmp,
        }
        self.inner.cmp(&other.inner)
    }
}

/// An `isotope` module
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Module {
    /// The name of this module
    name: SmolStr,
    /// The definitions in this module
    defs: Vec<(Spanned<Visibility>, Definition)>,
}

/// An `isotope` module visibility
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Visibility {
    /// Public visibility
    Public,
    /// Private visibility
    Private,
    /// Compilation-unit-level visibility
    Unit,
    /// Super-level visibility
    Super,
    /// Module-level visibility
    Module(SmolStr),
}

/// A definition in an `isotope` module
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Definition {
    /// A submodule
    Module(Arc<Spanned<Module>>),
    /// A function definition
    Function(Arc<Spanned<Function>>),
    //TODO: globals
    //TODO: constants
    //TODO: typedefs
    //TODO: opaque types
}

/// A function definition
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Function {
    /// The name of this function definition. It shares parameters with it's entry block!
    pub name: SmolStr,
    /// This function's return types
    pub return_ty: SmallVec<[Arc<Spanned<Type>>; 2]>,
    /// The number of states this function returns
    pub return_states: u32,
    /// This function's entry block
    pub entry: BasicBlock,
}

/// An `isotope` type
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Type {
    /// A type variable
    Var(SmolStr),
    /// A bitvector of integer length `n`
    Bits(u32),
    /// A struct
    Struct(Struct),
    /// A union
    Union(Union),
    /// A tagged union
    Enum(Enum),
    /// An array
    Array(Array),
}

/// An `isotope` struct
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Struct(pub SmallVec<[Arc<Spanned<Type>>; 2]>);

/// An `isotope` union
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Union(pub SmallVec<[Arc<Spanned<Type>>; 2]>);

/// An `isotope` tagged union
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Enum(pub SmallVec<[Arc<Spanned<Type>>; 2]>);

/// An `isotope` array type
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Array {
    /// The length of the array
    pub len: Arc<Spanned<Expr>>,
    /// The type of the array
    pub ty: Arc<Spanned<Type>>,
}

/// A basic block
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct BasicBlock {
    /// The name of this basic block
    name: SmolStr,
    /// This basic block's parameter list
    parameters: Vec<(SmolStr, Arc<Spanned<Type>>)>,
    /// The instructions making up this basic block
    inst: Vec<Instruction>,
    /// The terminator of this basic block.
    terminator: Option<Arc<Spanned<Terminator>>>,
}

/// An instruction
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Instruction {
    /// A local binding
    Local(Local),
    /// A nested basic block definition
    Block(Arc<Spanned<BasicBlock>>),
}

/// A local binding
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Local {
    /// The names being bound
    name: Vec<SmolStr>,
    /// The value being bound
    value: Arc<Spanned<Expr>>,
}

/// A terminator instruction
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Terminator {
    /// Return an expression
    Return(SmallVec<[Arc<Spanned<Expr>>; 4]>),
    /// Jump to a basic block
    Jump(Jump),
    /// A conditional branch
    Branch(Branch),
}

/// A jump to a basic block
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Jump {
    /// The target basic block name
    pub target: SmolStr,
    /// The jump's arguments
    pub args: SmallVec<[Arc<Spanned<Expr>>; 2]>,
}

/// A conditional branch
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Branch {
    branches: SmallVec<[(Option<UBig>, Option<Arc<Spanned<Terminator>>>); 1]>,
}

/// An expression
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Expr {
    /// An identifier
    Ident(SmolStr),
    /// A bitvector constant
    Bitvector(Bitvector),
    /// An application expression
    App(SmallVec<[Arc<Spanned<Expr>>; 3]>),
    /// A tuple expression
    Tuple(SmallVec<[Arc<Spanned<Expr>>; 3]>),
    /// An array expression
    Array(SmallVec<[Arc<Spanned<Expr>>; 3]>),
    /// A nested scope
    Scope(Scope),
}

/// A bitvector constant
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Bitvector {
    /// The width of this bitvector
    pub width: u32,
    /// The default display radix of this bitvector
    pub radix: Radix,
    /// The bits
    pub bits: UBig,
}

/// Default display radix for a bitvector
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Radix {
    /// Binary
    Bin = 2,
    /// Octal
    Oct = 8,
    /// Decimal
    Dec = 10,
    /// Hexadecimal
    Hex = 16,
}

/// A nested scope
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Scope {
    /// The list of instructions in this scope
    inst: Vec<Spanned<Instruction>>,
    /// The resulting value of this scope
    value: Arc<Spanned<Expr>>,
}
