/*!
Utility parsers for `isotope`
*/
use super::*;

/// A list of characters treated specially by `isotope`
pub const SPECIAL_CHARS: &'static str = "()[]{};\"\'#";

/// Parse an `isotope` identifier
///
/// This is defined to be any string of non-whitespace Unicode characters not containing one of [`SPECIAL_CHARS`] which does not start with a decimal digit.
/// 
/// # Examples
/// ```rust
/// # use isotope_ir_parser::parser::utils::*;
/// # use chumsky::Parser;
/// let ident = ident();
/// assert_eq!(ident.parse("x"), Ok("x".into()));
/// assert_eq!(ident.parse("var1"), Ok("var1".into()));
/// assert_eq!(ident.parse("事情"), Ok("事情".into()));
/// assert!(ident.parse("1var").is_err());
/// assert!(ident.parse("").is_err());
/// ```
//TODO: support "raw identifiers" containing special characters
pub fn ident() -> impl Parser<char, SmolStr, Error = Simple<char>> + Copy + Clone {
    filter(|c: &char| !c.is_digit(10) && !c.is_whitespace() && !SPECIAL_CHARS.contains(*c))
        .chain(filter(|c: &char| !c.is_whitespace() && !SPECIAL_CHARS.contains(*c)).repeated())
        .collect()
}

/// Parse a valid bitwidth for a bitvector type
///
/// This must be a decimal integer which fits in 32 bits.
///
/// # Examples
/// ```rust
/// # use isotope_ir_parser::parser::utils::*;
/// # use chumsky::Parser;
/// let bitwidth = bitwidth();
/// assert_eq!(bitwidth.parse("1"), Ok(1));
/// assert_eq!(bitwidth.parse("8"), Ok(8));
/// assert_eq!(bitwidth.parse("16"), Ok(16));
/// assert_eq!(bitwidth.parse("32"), Ok(32));
/// assert_eq!(bitwidth.parse("64"), Ok(64));
/// assert_eq!(bitwidth.parse("128"), Ok(128));
/// assert_eq!(bitwidth.parse("256"), Ok(256));
/// assert!(bitwidth.parse("4294967296").is_err());
/// ```
pub fn bitwidth() -> impl Parser<char, u32, Error = Simple<char>> + Copy + Clone {
    text::int(10)
        .try_map(|i: String, span| {
            u32::from_str_radix(i.as_ref(), 10)
                .map_err(|_err| Simple::custom(span, format!("Bit-width {i} >= 2**32")))
        })
        .labelled("bitwidth")
}

/// Parse a radix for a bitvector
///
/// This can be either `'b` for binary, `'o` for octal, `'d` for decimal, or `'h` or `'x` for hexadecimal.
///
/// # Examples
/// ```
/// # use isotope_ir_parser::parser::utils::*;
/// # use isotope_ir_parser::ast::*;
/// # use chumsky::Parser;
/// let radix = radix();
/// assert_eq!(radix.parse("'b"), Ok(Radix::Bin));
/// assert_eq!(radix.parse("'o"), Ok(Radix::Oct));
/// assert_eq!(radix.parse("'d"), Ok(Radix::Dec));
/// assert_eq!(radix.parse("'h"), Ok(Radix::Hex));
/// assert_eq!(radix.parse("'x"), Ok(Radix::Hex));
/// assert!(radix.parse("'t'").is_err());
/// ```
pub fn radix() -> impl Parser<char, Radix, Error = Simple<char>> + Copy + Clone {
    just('\'')
        .ignore_then(choice((
            just('b'),
            just('o'),
            just('d'),
            just('h'),
            just('x'),
        )))
        .map(|radix| match radix {
            'b' => Radix::Bin,
            'o' => Radix::Oct,
            'd' => Radix::Dec,
            'h' | 'x' => Radix::Hex,
            _ => unreachable!(),
        })
        .labelled("bitvector radix")
}

/// Parse a bitvector
///
/// As in Verilog, this is represented by a bitwidth, followed by a radix, followed by a string of digits
///
/// ```rust
/// # use isotope_ir_parser::parser::utils::*;
/// # use isotope_ir_parser::ast::*;
/// # use chumsky::Parser;
/// let bitvector = bitvector();
/// assert_eq!(
///     bitvector.parse("8'b01111110"),
///     Ok(Bitvector { width: 8, radix: Radix::Bin, bits: 0b1111110u32.into() })
/// );
/// assert!(bitvector.parse("8'b2").is_err());
/// ```
pub fn bitvector() -> impl Parser<char, Bitvector, Error = Simple<char>> + Copy + Clone {
    bitwidth().then(radix()).then_with(|(width, radix)| {
        text::digits(radix as u32).map(move |digits: String| Bitvector {
            width,
            radix,
            bits: UBig::from_str_radix(&digits[..], radix as u32).expect("Should never fail"),
        })
    })
}
