/*!
A parser for `isotope` IR
*/
use crate::ast::*;
use crate::Arc;
use chumsky::error::Simple;
use chumsky::prelude::*;
use chumsky::text;
use chumsky::text::whitespace;
use chumsky::text::Character;
use ibig::UBig;
use smol_str::SmolStr;

pub mod utils;
use utils::*;

/// Parse an `isotope` expression
///
/// Consumes whitespace to the *left*
pub fn expr(source_id: SourceId) -> impl Parser<char, Spanned<Expr>, Error = Simple<char>> {
    whitespace().ignore_then(atom(source_id).repeated().at_least(1).map_with_span(
        move |mut atoms, span| {
            if atoms.len() == 1 {
                atoms.remove(0)
            } else {
                Spanned {
                    span: (source_id, span),
                    inner: Expr::App(atoms.into_iter().map(Arc::new).collect()),
                }
            }
        },
    ))
}

/// Parse an atomic `isotope` expression.
///
/// Does not consume whitespace.
pub fn atom(source_id: SourceId) -> impl Parser<char, Spanned<Expr>, Error = Simple<char>> {
    recursive(|atom: Recursive<char, Spanned<Expr>, Simple<char>>| {
        choice((
            ident().map(Expr::Ident),
            bitvector().map(Expr::Bitvector),
            whitespace()
                .ignore_then(atom.clone())
                .repeated()
                .delimited_by(just('('), whitespace().ignore_then(just(')')))
                .map(|mut atoms| {
                    if atoms.len() == 1 {
                        atoms.remove(0).inner
                    } else {
                        Expr::App(atoms.into_iter().map(Arc::new).collect())
                    }
                }),
            whitespace()
                .ignore_then(atom.clone())
                .separated_by(just(',').padded())
                .allow_trailing()
                .delimited_by(just('{'), whitespace().ignore_then(just('}')))
                .map(|atoms| Expr::Tuple(atoms.into_iter().map(Arc::new).collect())),
                whitespace()
                    .ignore_then(atom)
                    .separated_by(just(',').padded())
                    .allow_trailing()
                    .delimited_by(just('['), whitespace().ignore_then(just(']')))
                    .map(|atoms| Expr::Array(atoms.into_iter().map(Arc::new).collect())),
        ))
        .map_with_span(move |inner, span| Spanned {
            span: (source_id, span),
            inner,
        })
    })
}
